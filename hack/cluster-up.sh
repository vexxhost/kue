#!/bin/bash

set -xe

# Start up the machines
vagrant up

# Grab all the IP addresses
MASTER_IP=$(vagrant ssh master -c "hostname -I | cut -d' ' -f1" 2>/dev/null | xargs echo -n)
MINION_1_IP=$(vagrant ssh minion-1 -c "hostname -I | cut -d' ' -f1" 2>/dev/null | xargs echo -n)
MINION_2_IP=$(vagrant ssh minion-2 -c "hostname -I | cut -d' ' -f1" 2>/dev/null | xargs echo -n)

# Start up an SSH agent
eval `ssh-agent -s`
ssh-add .vagrant/machines/master/libvirt/private_key
ssh-add .vagrant/machines/minion-1/libvirt/private_key
ssh-add .vagrant/machines/minion-2/libvirt/private_key
trap "ssh-agent -k" exit

tox -evenv -- kue-cli --user vagrant --master ${MASTER_IP} --minion ${MINION_1_IP} --minion ${MINION_2_IP}