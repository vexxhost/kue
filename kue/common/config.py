# Copyright 2019 VEXXHOST, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


class DashboardConfig(object):
    def __init__(self):
        self.enabled = True
        self.version = 'v1.10.1'


class FlannelConfig(object):
    def __init__(self):
        self.backend = 'vxlan'
        self.cidr = '10.244.0.0/16'
        self.subnet_length = 24


class ClusterConfig(object):
    def __init__(self):
        self.dashboard = DashboardConfig()
        self.flannel = FlannelConfig()

    @property
    def inventory(self):
        return {
            "all": {
                "children": {
                    "masters": {},
                    "minions": {},
                },
                "vars": {
                    "ansible_user": self.user,

                    # Flannel
                    "flannel_backend": self.flannel.backend,
                    "flannel_cidr": self.flannel.cidr,
                    "flannel_subnet_length": self.flannel.subnet_length,

                    # Dashboard
                    "kube_dashboard_enabled": self.dashboard.enabled,
                    "kube_dashboard_version": self.dashboard.version,
                }
            },
            "masters": {
                "hosts": {
                    node: {} for node in self.masters
                }
            },
            "minions": {
                "hosts": {
                    node: {} for node in self.minions
                }
            }
        }
