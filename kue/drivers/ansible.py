# Copyright 2019 VEXXHOST, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import shutil
import tempfile
import os

import ansible_runner

from kue import exceptions


class Cluster(object):
    def __init__(self, config):
        self.config = config

    def deploy(self):
        playbooks_dir = "%s/playbooks" % os.path.dirname(__file__)
        private_data_dir = tempfile.mkdtemp()

        try:
            shutil.copytree(playbooks_dir, '%s/project' % private_data_dir)
            result = ansible_runner.run(private_data_dir=private_data_dir,
                                        inventory=self.config.inventory,
                                        settings={'pexpect_use_poll': False},
                                        playbook='site.yaml')
            if result.status == 'failed' or \
                    (result.stats and result.stats.get('failures', [])):
                message = ' '.join(result.stdout)
                raise exceptions.AnsibleRunnerException(message)
        finally:
            shutil.rmtree(private_data_dir, ignore_errors=True)
