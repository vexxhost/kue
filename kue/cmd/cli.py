# Copyright 2019 VEXXHOST, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse

from kue.drivers import ansible
from kue.common import config

parser = argparse.ArgumentParser()

parser.add_argument('--user', type=str, required=True,
                    help='ssh username')
parser.add_argument('--master', type=str, required=True,
                    help='master')
parser.add_argument('--minion', type=str, required=True, action='append',
                    help='minion')


def main():
    args = parser.parse_args()

    cluster_config = config.ClusterConfig()
    cluster_config.user = args.user
    cluster_config.masters = [args.master]
    cluster_config.minions = args.minion

    cluster = ansible.Cluster(config=cluster_config)
    cluster.deploy()
